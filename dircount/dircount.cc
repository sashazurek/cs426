#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>
#define MAXPATHLEN 2048

std::unordered_set<ino_t> inodes;
long size_of_files = 0;

void dir(const char pathname[])
{
    auto dir_stream = opendir(pathname);
    std::string root_path = std::string(pathname);

    if (dir_stream == NULL) return;
    else
    {
        const std::unordered_map<unsigned char, char> dirent_type = {
            {DT_DIR,'d'}, // directory
            {DT_REG,'f'}, // file
            {DT_LNK,'l'}  // symbolic link
        };

        while (auto dir_entry = readdir(dir_stream))
        {
            struct stat stat_buf;
            int stat_ret = stat(dir_entry->d_name, &stat_buf);
            auto already_found = (inodes.find(stat_buf.st_ino) != inodes.end());
            bool not_dots = true;
            std::string pathmsg =
                root_path
                + "/"
                + std::string(dir_entry->d_name)
                + " ("
                + dirent_type.at(dir_entry->d_type);

            // test if dir is a .(.) and indicate if so
            if (std::string(dir_entry->d_name) == "..") not_dots = false;
            else if (std::string(dir_entry->d_name) == ".") not_dots = false;

            // keep track of unique _file_ inodes
            inodes.insert(stat_buf.st_ino);
            
            // assemble absolute current_path
            std::string cur_path = 
                root_path
                + '/'
                + std::string(dir_entry->d_name);

            // set up generic p
            switch (dir_entry->d_type)
            {
                case DT_REG:
                    size_of_files += stat_buf.st_size;
                    pathmsg += ( "," + std::to_string(stat_buf.st_size));
                    break;
                case DT_DIR:
                    if (not_dots) dir(cur_path.c_str());
                    break;
                case DT_LNK:{
                    char buf[MAXPATHLEN];
                    int len = readlink(cur_path.c_str(),buf,MAXPATHLEN);
                    buf[len] = 0;
                    pathmsg += ( ",->" + std::string(buf));
                    if (!already_found && S_ISDIR(stat_buf.st_mode)) 
                        dir(cur_path.c_str());
                    break;
                    }
                default: perror("Unsupported Type");
            }
            pathmsg += ")";
            printf("%s\n",pathmsg.c_str());
        }
    }
}

int main(int argc, char *argv[])
{
    assert(argv[1] != NULL);
    dir(argv[1]);
    printf("Size of all files in dir: %d\n",size_of_files);

    return 0;
}