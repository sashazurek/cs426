#define SIZE 5 
#include <condition_variable>
#include <iostream>
#include <mutex>

class RingBuffer
{
    private:
        // vars
        int bufsize = SIZE;
        int data[SIZE];
        int start = 0;
        int stop = 0;
        std::condition_variable cond;
        std::mutex mtx;
        
        // functions
        void increment_start();
        void decrement_start();
        void increment_stop();
        void decrement_stop();

    public:
        // functions
        char empty();
        char full();
        void put(int num);
        int get();
};
