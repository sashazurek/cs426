// condition_variable::notify_one
#include <iostream>           // cout
#include <thread>             // thread
#include <mutex>              // mutex, unique_lock
#include <condition_variable> // condition_variable
#include <unistd.h>		// usleep

using namespace std;

mutex mtx;
condition_variable produce,consume;

int cargo = 0;     // shared value by producers and consumers

void consumer () {
  
}

void producer () {
  
}

int main ()
{
  thread consumers[10],producers;
  // spawn 10 consumers and 10 producers:
  producers = thread(producer);
  for (int i=0; i<10; ++i) {
    consumers[i] = thread(consumer);
  }

  // join them back:
  for (int i=0; i<10; ++i) {
    consumers[i].join();
  }
  producers.join();

  return 0;
  }
