#include "Buffer.h"
#include <iostream>

void RingBuffer::increment_start()
{
    start = (start + 1) % bufsize;
}

void RingBuffer::increment_stop()
{
    stop = (stop + 1) % bufsize;
}

void RingBuffer::decrement_start()
{
    start = (start - 1) % bufsize;
}

void RingBuffer::decrement_stop()
{
    stop = (stop - 1) % bufsize;
}

char RingBuffer::empty()
{
    if (start == stop)
        return 1;

    return 0;
}
char RingBuffer::full()
{
    if ((stop + 1) % bufsize == start)
        return 1;

    return 0;
}

void RingBuffer::put(int num)
{
    
    std::unique_lock<std::mutex> lk(mtx);
    cond.wait(
            lk,
            [&]{return !full();}
            );
    data[stop] = num;
    increment_stop();
    cond.notify_one();
}

int RingBuffer::get()
{

    std::unique_lock<std::mutex> lk(mtx);
    cond.wait(
            lk,
            [&]{return !empty();}
            );
    increment_start();
    cond.notify_one();
    // math is faster than assigning, i hope
    return data[((start+bufsize)-1) % bufsize];
}
