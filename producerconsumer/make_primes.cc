/*Finish the code below.  It should start N examples of makePrimes.  It should start 1 example of print.
N comes from the command line as the first argument.
Basically, this means to finish main() to start and join threads, and to make Buffer class that
is a thread safe ring buffer that you wrote yourself.

You should not have to change any of my code below.

Atomics of any sort are forbidden.  Use must use mutexes, locks, notify, and wait.

Time this with 1 thread, 5 threads, and 10 threads.  Part of your homework is the timing data.

The program should search for primes from 100,000,001 to 100,001,001.

Hint:  You cannot call exit(0) when killing a program with running threads.  But you can call _exit(0).
Hint:  I thought the videos were really helpful for this program.

Points:
+1 Can start and join the threads
+1 Searches the right range
+1 Reads 'n' from the command line
+6 Has a thread safe ring buffer.
+1 Program terminates when done.

Due Mon Sep 22th.  -10% per workday late.

==============================================================*/
#include <cstdlib>
#include <chrono>
#include <iostream>           // cout
#include <thread>             // thread
#include <mutex>              // mutex, unique_lock
#include <condition_variable> // condition_variable
#include <unistd.h>// usleep
#include "Buffer.h"

using namespace std;

RingBuffer buffer;
int completes;
mutex completes_mtx;

bool isPrime(int maybe)
{
    if (maybe % 2 == 0)
        return false;

    for(int i = 3; i < maybe; i+=2)
    {
        if (maybe % i == 0)
            return false;
    }

    return true;
}

void makePrimes(int start, int stop)
{
    for(int i = start; i < stop; i++)
        if (isPrime(i)) buffer.put(i);

    lock_guard<mutex> guard(completes_mtx);
    completes -= 1;
}

void print()
{
    while (completes)
            cout << "Prime " << buffer.get() << endl; 
}

int main (int argc, char *argv[])
{
    const int start = 100000000;
    const int stop = 100001001;

    // determine thread count
    int thread_count = 1;
    if (argc != 1)
        thread_count = atoi(argv[1]);

    completes = thread_count;
    

    /* determine range of nums to be checked per thread
        since this is integer, we don't need to worry about the remainder --
        at least for the purposes of this assignment, and hopefully i'm
        not wrong... */
    int group_range = (stop - start) / thread_count;
    
    // initialize producer array
    thread producers[thread_count];

    // initialize producers
    for (int i = 0, j = thread_count; i < thread_count; i += 1)
    {
        j -= 1;
        producers[i] = thread(
                makePrimes, 
                (start + (group_range * i)), 
                (stop - (group_range * j))
                );
    }
    // start consumer, but don't join yet
    thread consumer = thread(print);
    // join producers
    for (int i = 0; i < thread_count; i += 1)
        producers[i].join();
    
    // join consumer
    consumer.join();

    return 0;
}
