#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

void error_and_close(int fds[], int argc, char * argv[], const char * errmsg, int errnum)
{
    // print err message
    perror(errmsg);

    // close valid fds
    for (int i = 1; i < argc; i++)
        if (fds[i]) close(fds[i]);

    // if output was created, remove empty file
    if (errnum != EEXIST) remove(argv[argc-1]);

    exit(errnum);
}

int main(int argc, char *argv[]) 
{
    int fds[argc];
    int destsize = 0;
    struct stat stats[argc];
    void * fileaddrs[argc];
    void * destptr;

    // check if valid no of args
    if (argc < 2) 
    {
        perror("Not enough args. Need >=1 input and 1 output.");
        exit(-1);
    }

    // check if output file exists
    fds[argc-1] = open(argv[argc-1], O_RDWR | O_CREAT | O_EXCL, 0666);

    if (fds[argc-1] < 0) 
        error_and_close(fds,argc,argv,"Invalid output file",EEXIST);

    // open input files
    for (int i = 1; i < argc-1; i++)
    {
        fds[i] = open(argv[i], O_RDONLY);
        if (fstat(fds[i], &stats[i]))
        {
            char errmsg[] = "Couldn't get info on file ";
            error_and_close(fds,argc,argv,strcat(errmsg, argv[i]),EBADF);
        }
        destsize += stats[i].st_size;
    }
    
    // map them into memory
    for (int i = 1; i < argc-1; i++)
    {
        fileaddrs[i] = mmap(NULL, stats[i].st_size, PROT_READ, MAP_PRIVATE, fds[i], 0);
        close(fds[i]);
        madvise(fileaddrs[i],stats[i].st_size,MADV_SEQUENTIAL);
    }

    // truncate dest file
    ftruncate(fds[argc-1], destsize);
    madvise(fileaddrs[argc-1],destsize,MADV_SEQUENTIAL);

    // map dest file
    fileaddrs[argc-1] = mmap(NULL, destsize, PROT_WRITE | PROT_READ, MAP_SHARED, fds[argc-1], 0);
    if (fileaddrs[argc-1] == MAP_FAILED)
        error_and_close(fds,argc,argv,"Couldn't map destination file\n",EBADF);
    
    // copy files into output
    destptr = fileaddrs[argc-1];
    for (int i = 1; i < argc-1; i++)
        destptr = mempcpy(destptr,fileaddrs[i],stats[i].st_size);

    // close fds
    for (int i = 1; i < argc; i++)
        close(fds[i]);

    return 0;
}
