#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <readline/readline.h>
#include <readline/history.h>
#include <string>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <signal.h>
#include <wordexp.h>

// from https://brennan.io/2015/01/16/write-a-shell-in-c/
#define TOK_DELIM " \t\r\n\a"
#define TOK_BUFSIZE 64

struct Config {
    const char * histfile; 
};

const char * default_prompt = "user@hostname: ";

// if $PS1 is set, use that for the prompt
// otherwise, return default prompt
char * get_prompt()
{
    char * try_PS1 = getenv("PS1");
    if (try_PS1)
        return try_PS1; // this should be user env PS1

    return (char *) default_prompt;
}

void initialize_config(struct Config *config) {
    config -> histfile = "./.history";
}

void allocation_error(const char * caller)
{
    perror("allocation error in");
    perror(caller);
    exit(errno);
}

// inspired by https://brennan.io/2015/01/16/write-a-shell-in-c/
char **split_line(char *line, char* splitter)
{
    int pos = 0;
    int bufsize = TOK_BUFSIZE;
    char ** tokens = new char*[bufsize];
    char *token;

    if (!tokens) allocation_error("split_line()");

    token = strtok(line, splitter);
    while (token != NULL)
    {
        tokens[pos] = token;
        pos++;
        
        // if we overrun the buffer, reallocate extra memory
        if (pos >= bufsize)
        {
            bufsize += TOK_BUFSIZE;
            tokens = new char*[bufsize];
            if (!tokens) allocation_error("split_line()");
        }
        token = strtok(NULL, splitter);
    }
    // terminate the final string
    tokens[pos] = NULL;
    return tokens;
}

// if we get SIGINT, reset the prompt
static void interrupt_handler(int sig)
{
    printf("\n");
    rl_on_new_line();
    rl_replace_line("",0);
    rl_redisplay();
}

// set one environment variable
void set_env(char * &word)
{
    char * env_var[2];
    env_var[0] = strtok(word,"=");
    env_var[1] = strtok(NULL,"=");

    switch (setenv(env_var[0],env_var[1],1))
    {
        case 0:
            break;
        default:
            allocation_error("set_envs()");
    }
}

// inspired by https://www.gnu.org/software/libc/manual/html_node/Wordexp-Example.html
// returns 0 on success, -1 on failure
int expand_command(char * program, char ** args, wordexp_t &result)
{
    switch (wordexp(program, &result, 0))
    {
        case 0:
            break;
        case WRDE_NOSPACE:
            wordfree(&result);
        default:
            perror("Unspecified wordexp error");
            return -1;
    }

    for (int i = 1; args[i] != NULL; i++)
    {
        if (wordexp(args[i], &result, WRDE_APPEND))
        {
            wordfree(&result);
            perror("Unspecified wordexp error");
            return -1;
        }
    }
    return 0;
}

void execution(char ** &parsed_input)
{
    int child, status;
    int pid = fork();

    if (pid == 0)
    {
        wordexp_t command;
        if (expand_command(parsed_input[0],parsed_input,command))
        {
            printf("Couldn't expand command -- likely allocation failure");
            exit(errno);
        }

        execvp(command.we_wordv[0],command.we_wordv);
        printf("Error: Couldn't launch %s\n", command.we_wordv[0]); 
        // printf("Did you mean: %s?\n", closest_match(parsed_input[0]));
        exit(-1);
    }
    else if (pid > 0)
    {
        do
        // inspired by https://brennan.io/2015/01/16/write-a-shell-in-c/
            auto wpid = waitpid(pid, &status,WUNTRACED);
        while (!WIFEXITED(status) && !WIFSIGNALED(status));
    }
}

void piped_command(char ** parsed_input)
{
    char *** commands[2];

    // parse the first command and store into commands[0]
    // parse the second command
    // do the forks and stuff
}

void run_file(struct Config &config, char * fname);
bool evaluate_cmd(char ** parsed_input, struct Config &config, char * input);

// parse input into words
bool process_a_line(struct Config &config, char * input, bool is_user_input)
{
    bool status = true;
    if (strcmp(input,"")) // ignore empty line
    {
        // history management
        if (is_user_input)
        {
            add_history(input);
            write_history(config.histfile);
        }
        if (strchr(input,'=') != NULL)
            set_env(input);
        else 
        {
            char ** parsed_input;
            // if we have history expansion to do
            if (!strncmp(input,"!",1))
            {
                switch(history_expand(input,parsed_input))
                {
                    case 0:
                    case 1:
                        break;
                    case -1:
                        printf("error: %s", parsed_input[0]);
                        break;
                    case 2:
                        for (int i = 0; parsed_input[i] == NULL; i++)
                            printf("%s",parsed_input[i]);
                        printf("\n");
                        break;
                    default:
                        perror("history expand failed in an undefined way");
                }
            }
            else if (strchr(input,';') != NULL)
            {
                parsed_input = split_line(input,";");
                int commands = *(&parsed_input + 1) - parsed_input;
                for (int i = 0; parsed_input[i] != NULL; i++)
                {
                    char ** cmd = split_line(parsed_input[i], TOK_DELIM);
                    status = evaluate_cmd(cmd,config,parsed_input[i]);
                }
            }
            else
            {
                parsed_input = split_line(input, TOK_DELIM);
            }
            status = evaluate_cmd(parsed_input,config, input);
        }
    }
    return status;
}
bool evaluate_cmd(char ** parsed_input, struct Config &config, char * input)
{
    // exit command
    if (!strcmp(parsed_input[0],"exit")) 
        return false;
    else if (!strcmp(parsed_input[0],"cd"))
        chdir(parsed_input[1]);
    else if (strchr(input,'.') != NULL)
        run_file(config, parsed_input[1]);
    else
    {
        bool is_piped = false;
        for (int i = 0; parsed_input[i] != NULL; i++)
        {
            if (!strcmp(parsed_input[i],"|"))
                is_piped = true;
        }
        if (is_piped)
        {
            for (int i = 0; parsed_input[i] != NULL; i++)
                printf("[%d]: %s\n",i,parsed_input[i]);
            piped_command(parsed_input);
        }
        else
            execution(parsed_input);
    }
    return true;
}

void run_file(struct Config &config, char * fname)
{
    FILE * user_setup = fopen(fname,"r");
    // if we couldn't do it, don't worry about it
    if (!user_setup) return;
    // for each line in .myshell
    char buf[128];

    while (fgets(buf,sizeof buf, user_setup) != NULL)
    {
        char * newline = strstr(buf,"\n");
        if (newline != NULL)
            strncpy(newline, "\0", 1);
        process_a_line(config,buf,false);
    }
    fclose(user_setup);
}

int main()
{
    struct Config config;
    bool active = true;

    // initialization
    initialize_config(&config);
    run_file(config,"./.myshell");
    using_history();
    read_history(config.histfile);
    signal(SIGINT,interrupt_handler);
    
    while (
        process_a_line(
            config,
            readline(get_prompt()),
            true
            )
        );
    
    return 0;
}
