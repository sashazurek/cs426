import java.util.Random;
import java.lang.String;
import java.util.ArrayList;

class Calcpi
{
    public static void main (String [] args)
    {
        int thread_count = Integer.parseInt(args[0]);
        int total_throws = 299511627;
        Counter counter = new Counter();
        ArrayList<PiThread> threads = new ArrayList<PiThread>();

        // start threads
        for (int i = 0; i < thread_count; i++)
        {
            threads.add(new PiThread(counter, total_throws/thread_count));
            threads.get(i).start();
        }
        
        try
        {
            for (PiThread thread : threads)
            {
                thread.join();
            }
        }
        catch (InterruptedException e)
        {
            System.out.println("Uh oh.");
        }
        //print "Pi = " hits / total * 4
        System.out.println(
                "Pi is: "
                + 4.0*(Double.valueOf(counter.hits)
                    / total_throws)
                );
    }
}

class Counter
{
    int hits;
}

class PiThread extends Thread
{
    Random rand = new Random();
    Counter counter = new Counter();
    int dart_throws = 0;

    public PiThread(Counter counter, int dart_throws) 
    {
        this.counter = counter;
        this.dart_throws = dart_throws;
    }

    public void run ()
    {
        int local_hits = 0;
        // run until desired accuracy
        for (int i = 0; i < this.dart_throws; i++)
        {
            //throw a dart randomly and uniformly over the unit square
            Double dart_x = rand.nextDouble();
            Double dart_y = rand.nextDouble();

            //compute it's distance from the center
            Double length = (dart_x*dart_x) + (dart_y*dart_y);

            if (length <= 1)
                local_hits += 1;
        }
        counter.hits += local_hits; 
    }
}
