let SessionLoad = 1
if &cp | set nocp | endif
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
silent only
silent tabonly
cd ~/code/cs426/calcpi
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
argglobal
%argdel
$argadd ~/code/cs426/calcpi
edit Calcpi.java
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
wincmd _ | wincmd |
split
1wincmd k
wincmd _ | wincmd |
vsplit
wincmd _ | wincmd |
vsplit
2wincmd h
wincmd w
wincmd w
wincmd w
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
exe 'vert 1resize ' . ((&columns * 84 + 154) / 309)
exe '2resize ' . ((&lines * 31 + 31) / 63)
exe 'vert 2resize ' . ((&columns * 76 + 154) / 309)
exe '3resize ' . ((&lines * 31 + 31) / 63)
exe 'vert 3resize ' . ((&columns * 77 + 154) / 309)
exe '4resize ' . ((&lines * 31 + 31) / 63)
exe 'vert 4resize ' . ((&columns * 69 + 154) / 309)
exe '5resize ' . ((&lines * 27 + 31) / 63)
exe 'vert 5resize ' . ((&columns * 115 + 154) / 309)
exe '6resize ' . ((&lines * 27 + 31) / 63)
exe 'vert 6resize ' . ((&columns * 108 + 154) / 309)
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 45 - ((44 * winheight(0) + 29) / 59)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
45
normal! 026|
wincmd w
argglobal
if bufexists("examples/CalcPI1.java") | buffer examples/CalcPI1.java | else | edit examples/CalcPI1.java | endif
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 37 - ((21 * winheight(0) + 15) / 31)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
37
normal! 0
lcd ~/code/cs426/calcpi
wincmd w
argglobal
if bufexists("~/code/cs426/calcpi/examples/CalcPI2.java") | buffer ~/code/cs426/calcpi/examples/CalcPI2.java | else | edit ~/code/cs426/calcpi/examples/CalcPI2.java | endif
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 34 - ((25 * winheight(0) + 15) / 31)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
34
normal! 07|
lcd ~/code/cs426/calcpi
wincmd w
argglobal
if bufexists("~/code/cs426/calcpi/examples/CalcPI3.java") | buffer ~/code/cs426/calcpi/examples/CalcPI3.java | else | edit ~/code/cs426/calcpi/examples/CalcPI3.java | endif
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 36 - ((25 * winheight(0) + 15) / 31)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
36
normal! 0
lcd ~/code/cs426/calcpi
wincmd w
argglobal
terminal ++curwin ++cols=115 ++rows=27 
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
let s:l = 1 - ((0 * winheight(0) + 13) / 27)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
1
normal! 0
wincmd w
argglobal
if bufexists("~/code/cs426/calcpi/compute.pi.txt") | buffer ~/code/cs426/calcpi/compute.pi.txt | else | edit ~/code/cs426/calcpi/compute.pi.txt | endif
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 3 - ((2 * winheight(0) + 13) / 27)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
3
normal! 0
wincmd w
exe 'vert 1resize ' . ((&columns * 84 + 154) / 309)
exe '2resize ' . ((&lines * 31 + 31) / 63)
exe 'vert 2resize ' . ((&columns * 76 + 154) / 309)
exe '3resize ' . ((&lines * 31 + 31) / 63)
exe 'vert 3resize ' . ((&columns * 77 + 154) / 309)
exe '4resize ' . ((&lines * 31 + 31) / 63)
exe 'vert 4resize ' . ((&columns * 69 + 154) / 309)
exe '5resize ' . ((&lines * 27 + 31) / 63)
exe 'vert 5resize ' . ((&columns * 115 + 154) / 309)
exe '6resize ' . ((&lines * 27 + 31) / 63)
exe 'vert 6resize ' . ((&columns * 108 + 154) / 309)
tabnext 1
badd +34 ~/code/cs426/calcpi/Calcpi.java
badd +0 ~/code/cs426/calcpi
badd +37 ~/code/cs426/calcpi/examples/CalcPI1.java
badd +34 ~/code/cs426/calcpi/examples/CalcPI2.java
badd +36 ~/code/cs426/calcpi/examples/CalcPI3.java
badd +1 ~/code/cs426/calcpi/compute.pi.txt
badd +1 ~/code/cs426/calcpi/calcpi.java
badd +1 ~/code/cs426/calcpi/CalcPI2
badd +1 ~/code/cs426/calcpi/CalcPI2.java
badd +1 ~/code/cs426/calcpi/examples/CalcPI3.class
badd +0 ~/code/cs426/.git/index
if exists('s:wipebuf') && len(win_findbuf(s:wipebuf)) == 0
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToOSAc
set winminheight=1 winminwidth=1
let s:sx = expand("<sfile>:p:r")."x.vim"
if filereadable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save
nohlsearch
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
